<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => 'site.auth'], function() {
    Route::get('/', 'Front\SiteController@index');
    Route::get('/admin', 'Front\SiteController@admin');
    Route::get('/logout', 'Back\AuthController@logout');
    Route::get('/get-bonus', 'Front\SiteController@getBonus');
});
Route::group(['middleware' => 'site.guest'], function() {
    Route::get('/login', 'Back\AuthController@loginForm');
    Route::post('/login', 'Back\AuthController@login');
    Route::get('/register', 'Back\AuthController@registerForm');
    Route::post('/register', 'Back\AuthController@register');
});

