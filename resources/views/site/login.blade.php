@extends('layouts.auth')
@section('content')
    <div class="card mt-3">
        <h5 class="card-header text-center">Вход</h5>
        <div class="card-body">
            @include('includes.errors')
            <form action="/login" method="post">
                {{csrf_field()}}
                <div class="form-group">
                    <label for="exampleInputEmail1">Email адрес</label>
                    <input type="email" name="email" value="{{old('email')}}" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Пароль</label>
                    <input type="password" name="password" class="form-control" id="exampleInputPassword1">
                </div>
                <button type="submit" class="btn btn-primary">Войти</button>
            </form>
        </div>
        <div class="card-footer text-center">
            <a href="/register">Нет аккаунта ?</a>
        </div>
    </div>
@endsection
