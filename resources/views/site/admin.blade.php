@extends('layouts.master')
@section('content')
    <div class="stat">
        <h5 class="pb-2 mt-4 mb-2 border-bottom">Призы</h5>
        <table class="table table-striped">
            <thead class="thead-dark text-center">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Пользователь</th>
                <th scope="col">Значение</th>
                <th scope="col">Тип приза</th>
                <th scope="col">Состояние</th>
                <th scope="col">Дата</th>
            </tr>
            </thead>
            <tbody class="text-center">
            @php $i = 1; @endphp
            @foreach($wins as $win)
                <tr>
                    <td>{{$i}}</td>
                    <td>{{$win->user->name}}</td>
                    <td>{{$win->prize->value}}</td>
                    <td>{{$win->prize->category->name}}</td>
                    <td>{{$win->status->name}}</td>
                    <td>{{$win->created_at}}</td>
                </tr>
                @php $i++; @endphp
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
