@if($errors->all())
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
@elseif(session()->has('message'))
    <div class="alert alert-danger">
        <ul>
            <li>{{session()->get('message')}}</li>
        </ul>
    </div>
@endif

