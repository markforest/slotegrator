<?php

namespace App;

use App\Models\Win;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // Свойство зависимостей с таблицей wins
    public function wins()
    {
        return $this->hasMany(Win::class);
    }

    // Создание нового пользователя
    public static function add($fields)
    {
        $user = new static;
        $user->fill($fields);
        $user->password = User::preparePassword($fields['password']);
        $user->save();
        return $user;
    }

    // Хеширование пароля
    public static function preparePassword($password)
    {
        return Hash::make($password);
    }
}
