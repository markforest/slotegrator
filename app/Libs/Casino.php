<?php


namespace App\Libs;


use App\Models\Category;
use App\Models\Prize;
use App\Models\Reserve;
use App\Models\Win;
use Illuminate\Support\Facades\Auth;

class Casino
{
    private $prizes_entity = ['Машина', 'Квартира', 'Ноутбук'];
    public function generatePrize()
    {
        $category_ids = Category::all()->pluck('id')->toArray();
        $random_category_id = $category_ids[rand(0, count($category_ids)-1)];
        $random_category_name = Category::find($random_category_id)->name;
        $value = '';
        switch ($random_category_name) {
            case 'физический предмет': {
                $random_entity = $this->prizes_entity[rand(0, count($this->prizes_entity)-1)];
                $value = $random_entity;
                $reserve = Reserve::all()->where('category_id', $random_category_id)->first();
                $reserve->value = ($reserve->value - 1);
                $reserve->save();
                break;
            }
            case 'денежный': {
                $value = rand(1, 1000);
                $reserve = Reserve::all()->where('category_id', $random_category_id)->first();
                $reserve->value = ($reserve->value - $value);
                $reserve->save();
                break;
            }
            default: {
                $value = rand(1, 1000);
            }
        }
        $prize = Prize::add([
            'category_id' => $random_category_id,
            'value' => $value,
        ]);
        $win = Win::add([
            'user_id' => Auth::user()->id,
            'prize_id' => $prize->id,
            'status_id' => 1
        ]);

        return redirect()->back();
    }
}
