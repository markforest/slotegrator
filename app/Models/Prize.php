<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Prize extends Model
{
    // Таблица соответствия из базы
    protected $table = 'prizes';

    // Поля для создания новой записи используя метод fill
    protected $fillable = ['category_id', 'value'];

    // Свойство зависимостей с таблицей categories
    public function category()
    {
        return $this->hasOne(Category::class, 'id', 'category_id');
    }

    // Свойство зависимостей с таблицей wins
//    public function wins()
//    {
//        return $this->hasMany(Win::class);
//    }

    // Создание приза
    public static function add($fields)
    {
        $prize = new static;
        $prize->fill($fields);
        $prize->save();
        return $prize;
    }
}
