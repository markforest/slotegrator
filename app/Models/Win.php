<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Win extends Model
{
    // Таблица соответствия из базы
    protected $table = 'wins';

    // Поля для создания новой записи используя метод fill
    protected $fillable = ['user_id', 'prize_id', 'status_id'];

    // Свойство зависимостей с таблицей statuses
    public function status()
    {
        return $this->hasOne(Status::class, 'id', 'status_id');
    }

    // Свойство зависимостей с таблицей prizes
    public function prize()
    {
        return $this->hasOne(Prize::class, 'id', 'prize_id');
    }

    // Свойство зависимостей с таблицей users
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    // Создание сущности
    public static function add($fields)
    {
        $win = new static;
        $win->fill($fields);
        $win->save();
        return $win;
    }
}
