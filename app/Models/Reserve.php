<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reserve extends Model
{
    // Таблица соответствия из базы
    protected $table = 'reserves';

    // Поля для создания новой записи используя метод fill
    protected $fillable = ['category_id', 'value'];

    // Свойство зависимостей с таблицей categories
    public function category()
    {
        return $this->hasOne(Category::class);
    }
}
