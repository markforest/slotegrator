<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    // Таблица соответствия из базы
    protected $table = 'categories';

    // Поля для создания новой записи используя метод fill
    protected $fillable = ['name', 'is_limited'];

    // Свойство зависимостей с таблицей prizes
    public function prizes()
    {
        return $this->hasMany(Prize::class);
    }

    // Свойство зависимостей с таблицей reserves
    public function reserves()
    {
        return $this->hasMany(Reserve::class);
    }
}
