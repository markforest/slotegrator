<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    // Таблица соответствия из базы
    protected $table = 'statuses';

    // Поля для создания новой записи используя метод fill
    protected $fillable = ['name'];

    // Свойство зависимостей с таблицей wins
    public function wins()
    {
        return $this->hasMany(Win::class);
    }
}
