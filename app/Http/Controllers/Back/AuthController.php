<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function loginForm(Request $request)
    {
        return view('site.login');
    }

    public function registerForm(Request $request)
    {
        return view('site.register');

    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required'
        ]);

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            return redirect('/');
        } else {
            return redirect()->back()->with('message', 'Регистрация не удалась.');
        }
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'password' => 'required',
            'email' => 'required|unique:users',
        ]);
        $user = User::add($request->all());
        if($user) {
            Auth::login($user);
            return redirect('/');
        } else {
            return redirect()->back()->with('message', 'Регистрация не удалась.');
        }
    }

    public function logout(Request $request)
    {
        Auth::logout();
        return redirect('/login');
    }
}
