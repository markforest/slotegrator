<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Libs\Casino;
use App\Models\Win;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SiteController extends Controller
{
    public function index(Request $request)
    {
        $wins = Auth::user()->wins;
        return view('site.index', [
            'name' => Auth::user()->name,
            'page' => 'main',
            'wins' => $wins,
        ]);
    }

    public function admin(Request $request)
    {
        $wins = Win::all();
        return view('site.admin', [
            'name' => Auth::user()->name,
            'page' => 'admin',
            'wins' => $wins,
        ]);
    }

    public function getBonus(Request $request)
    {
        $casino = new Casino();
        $casino->generatePrize();
        return redirect('/');
    }
}
